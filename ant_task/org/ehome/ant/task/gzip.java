package org.ehome.ant.task;
import java.io.*;
import java.util.zip.GZIPOutputStream;
import java.util.Vector;
import java.util.Iterator;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.types.FileSet;

public class gzip extends Task {

    private Vector filesets = new Vector();

    public void addFileset(FileSet fileset) {
        filesets.add(fileset);
    }

    protected void validate() {
        if (filesets.size()<1) throw new BuildException("fileset not set");
    }

    public void execute() {
        validate();                                                             // 1
        for(Iterator itFSets = filesets.iterator(); itFSets.hasNext(); ) {      // 2
            FileSet fs = (FileSet)itFSets.next();
            DirectoryScanner ds = fs.getDirectoryScanner(getProject());         // 3
            String[] includedFiles = ds.getIncludedFiles();
            for(int i=0; i<includedFiles.length; i++) {
                File base  = ds.getBasedir();
                File file = new File(base, includedFiles[i]);
//                System.out.println(file.getAbsolutePath());
//                System.out.println(base);
//                System.out.println(includedFiles[i]);
                gzipFile(file, base, includedFiles[i]);
            }
        }
    }

    private void gzipFile(File file, File baseDir, String originName){
        try{
            BufferedReader in = new BufferedReader(new FileReader(file));
            BufferedOutputStream out = new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(new File(baseDir, originName+".gz"))));
            int c;
            while ((c = in.read()) != -1)
              out.write(c);
            in.close();
            out.close();
        }catch(Exception ex){
            ex.printStackTrace();
            throw new BuildException("Error when GZip file:"+ex.getMessage());
        }
    }
}