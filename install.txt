software:

XAMPP
http://www.apachefriends.org/en/xampp-windows.html
with apache 2.2 or 2.4

Ant
http://ant.apache.org/
version 1.9.0






XAMPP settings:

to host file add next line "192.168.1.118 ehw"    

to ...\xampp\apache\conf\extra\httpd-vhosts.conf 

add next config:

for apache 2.2 uncomment: NameVirtualHost *:80


#default xampp site with default dir "D:/work/xampp/htdocs"
<VirtualHost *:80>
    DocumentRoot "D:/work/xampp/htdocs"
    ServerName localhost
    ErrorLog "logs/error.log"
    CustomLog "logs/access.log" combined

    <Directory "D:/work/xampp/htdocs">
        AllowOverride All
        Order Allow,Deny
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>

#virtual host ehw for html source for desktop browser

<VirtualHost *:80>
    DocumentRoot "D:/temp/ehome/client/html/source/"
    ServerName ehw
    ErrorLog "logs/ehw.localhost-error.log"
    CustomLog "logs/ehw.localhost-access.log" combined

    <Directory "D:/temp/ehome/client/html/source/">
        AllowOverride All
        Order Allow,Deny
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>



copy content of Ant distr to "ant" directory (near "client" dir)


change arduino dir in build.bat 


first run "build.bat init"

to make build run "build.bat"