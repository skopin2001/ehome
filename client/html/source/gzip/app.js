(function (){
    var self=this;

    var app=app || {
        init: function(){
            _log("APP INIT");

            $("#testi18n").i18n();
        },
        start: function(){
            _log("APP START");
        }

    }
    window.app=app;

}).call(window);
