(function (){
    var self=this;

    var loader=loader || {

        maxTryCount: 5,

        scripts: [ //first element (must be jQuery) must have remote and local property, also type===js!!!
            {
                name: "jQuery-min-1.8.2.js",
                type: "js",
                local: "/gzip/jq.js",
                remote: "http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"
            },
            {
                name: "eHome.css",
                type: "css",
                local: "/html/ehome.css"
            },
            {
                name: "jQueryMobile-min-1.3.0.css",
                type: "css",
                local: "/gzip/jqm.css",
                remote: "http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css"
            },
            {
                name: "jQueryMobile-min-1.3.0.js",
                type: "js",
                local: "/gzip/jqm.js",
                remote: "http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"
            },
            {
                name: "app.js",
                type: "js",
                local: "/gzip/app.js"
            }
        ],
        
        init: function(){
            _log("loader init");
            _log("load lang="+window.i18n_lang);

            var self=this;

            if(window.i18n_lang){
                var lang_script={
                        name: "Lang pack for "+window.i18n_lang,
                        type: "js",
                        local: "/gzip/lang_"+window.i18n_lang+".js"
                    };

                this.scripts.splice(1, 0, lang_script); //after jQuery
            }

            var i18e_script={
                    name: "i18next EXTEND",
                    type: "js",
                    local: "/gzip/i18ne.js",
                    loadCallback: function(){
                        loader.initI18N();
                    }
                };

            this.scripts.splice(2, 0, i18e_script); //after lang pack


            this.loadScripts();
        },

        initI18N: function(){
            _log("init I18N");

            var option = { 
                load: 'current'
            }

            if(window.i18nLangPack){
                option["lng"]=window.i18nLangPack.lang;
                option["resStore"]=window.i18nLangPack.content;
            }

            i18n.init(option);

            window.i18next_extend.addJqueryFunct(i18n.options, i18n.translate);

        },


        randomFromInterval: function(from,to)
        {
            return Math.floor(Math.random()*(to-from+1)+from);
        },

        loadScripts: function(){
            _log("loader loadOtherScripts");

            var self=this;
            YUI().use('get', function (Y) {
                self.loadScript(Y, 0);
            });
            
        },

        loadScript: function(Y, index){
            _log("loader loadScript with index="+index);

            var self=this;
            if(index < self.scripts.length){
                if(!self.scripts[index].loaded){
                    self.tryLoadScript(Y, index, 1, function(err){
                        if(err){
                            self.errorLoadingScripts();
                        }else{
                           setLoadingPercent(self.getPercent(index+1));
                           index++;
                           self.loadScript(Y, index);
                        }
                    });
                }else{
                    setLoadingPercent(self.getPercent(index+1));
                    index++;
                    self.loadScript(Y, index);
                }
            }else{
                _log("loader loadScript complete!");

                self.successLoadingScripts();
                return;
            }
        },

        getPercent: function(index, tryCount){
            var l=this.scripts.length;
            return this.roundNumber((((index+1)*80/l)+(tryCount?(80/l*(tryCount/this.maxTryCount)):0)),0);
        },

        roundNumber: function(number, digits) {
            var multiple = Math.pow(10, digits);
            var rndedNum = Math.round(number * multiple) / multiple;
            return rndedNum;
        },


        successLoadingScripts: function(){
            var self=this;

            _log("loader successLoadingScripts");
            setLoadingPercent(100);

            setTimeout(function(){
                $("#progressbar").hide();
                $(".main").show();
                self.startMain();
            },200);
        },

        errorLoadingScripts: function(){
            _log("loader errorLoadingScripts");
            setLoadingPercent(0);

            setTimeout(function(){
                $("#progressbar").hide();
                $(".errorLoadingScript").show();
            },500);

        },


        tryLoadScript: function(Y, index, tryCount, callback){
            _log("loader tryLoadScript with index="+index+" and tryCount="+tryCount);

            var self=this;
            var script=self.scripts[index];

            var _url=tryCount===1?script.remote?self.isOffline?script.local:script.remote:script.local:script.local;
                
            var timeout=self.randomFromInterval(100,300);

            if(tryCount < self.maxTryCount){
            
                setTimeout(function(){
                    Y.Get[script.type](_url, {timeout: 2000}, function (err) {
                        if (err) {
                            _log('Error loading ' + script.name+" "+err[0].error);

                            if(_url===script.remote){self.isOffline=true; _log("set isOffile flag");}

                            setLoadingPercent(self.getPercent(index, tryCount+1));

                            self.tryLoadScript(Y, index, tryCount+1, callback);
                        }else{
                            _log(script.name+" "+'loaded successfully!');

                            if(script.loadCallback){
                                script.loadCallback();
                            }
                            callback(false);
                        }
                    });
                },timeout);
            }else{
                callback(true);
            }
        },

        startMain: function(){
            _log("start APP");

            app.init();
            app.start();
        }
        
    };

    window._log=function(message){

        if(console){
            console.log(message);
        }
        if(logToHtml){
            logToHtml(message);
        }

    };

    window.loader=loader;
    loader.init();

}).call(window);
